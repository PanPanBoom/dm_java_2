import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if(liste.size() == 0) // Verifie que la liste n'est pas vide
            return null;

        Integer min = liste.get(0);
        for(Integer x : liste)
            if(x < min)
                min = x;
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T x : liste)
            if(valeur.compareTo(x) >= 0) // Si on trouve un élément plus petit ou égal à valeur, on peut directement retourner false
                return false;
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res = new ArrayList<>();
        
        int i = 0;
        int j = 0;

        while(i < liste1.size() && j < liste2.size()) // La boucle continue tant que les deux indices peuvent parcourir leur liste
        {
            if(liste1.get(i).compareTo(liste2.get(j)) == 0 && !res.contains(liste1.get(i))) // Si les deux éléments sont égaux, j'ajoute l'élément à res et j'incrémente les deux indices
            {
                res.add(liste1.get(i));
                i++;
                j++;
            }
	    
	    // Sinon je vérifie quel élément est plus grand, et j'incrémente un indice en conséquence
            else if(liste1.get(i).compareTo(liste2.get(j)) > 0)
                j++;

            else
                i++;
        }

        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<>();
        String[] words = texte.split(" "); // J'utilise split avec l'espace pour séparer tous les mots
        for(String w : words)
            if(!w.equals("")) // Puis j'ajoute tous les mots à res, sauf ceux provoquer par plusieurs espaces
                res.add(w);

        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        Map<String, Integer> freq = new HashMap<>();
        List<String> words = decoupe(texte); // J'utilise la fonction crée juste au dessus pour séparer les mots

        for(String w : words) // Ici je remplis mon dictionnaire de fréquences
        {
            freq.putIfAbsent(w, 0);
            freq.replace(w, freq.get(w) + 1);
        }

        String word = null;
        Integer max = 0;

        for(String k : freq.keySet()) // Et ici je fais un algorithme max sur le dictionnaire de fréquences
            if(freq.get(k) > max || (freq.get(k) == max && word.compareTo(k) > 0))
            {
                max = freq.get(k);
                word = k;
            }

        return word;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int nbOpen = 0;
        for(int i = 0;i<chaine.length();i++) // Je parcours par indice, pour pouvoir parcourir chaque caractère du String
        {
            if(chaine.charAt(i) == '(') // Si une parenthèse s'ouvre, je l'ajoute au compteur
                nbOpen += 1;
            else if(chaine.charAt(i) == ')') // et si elle se ferme, je vérifie que le compteur est au dessus de 0, sinon ce n'est pas possible, et je décrémente le compteur
            {
                if(nbOpen > 0)
                    nbOpen -= 1;
                else
                    return false;
            }
        }

        return nbOpen == 0; // Puis je retourne true si toutes les parenthèses ont été fermé, donc si le compteur vaut 0
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
	/*
		Dans cet algo, je préfère utiliser une liste plutôt que plusieurs compteurs, car ce sera plus
		pratique pour vérifier si c'est une parenthèse ou un crochet qui a été ouvert dans l'ordre
	*/
        List<Character> lstOpen = new ArrayList<>();
        char charI;

        for(int i = 0; i < chaine.length(); i++)
        {
            charI = chaine.charAt(i); // Je stocke le caractère courant dans une variable, pour des raisons d'optimisations, car on l'appelle souvent
            if(charI == '(' || charI == '[') // Si c'est un caractère ouvrant, peu importe lequel, je l'ajoute à la liste
                lstOpen.add(charI);

	    // Sinon, je vérifie le caractère qui ferme, et je vérifie s'il correspond à celui qui ouvre, ou même s'il y a un caractère qui ouvre (donc size > 0)
            else if(charI == ']')
            {
                if(lstOpen.size() == 0 || !lstOpen.get(lstOpen.size() - 1).equals('['))
                    return false;
                else
                    lstOpen.remove(lstOpen.size() - 1);
            }

            else if(charI == ')')
            {
                if(lstOpen.size() == 0 || !lstOpen.get(lstOpen.size() - 1).equals('('))
                    return false;
                else
                    lstOpen.remove(lstOpen.size() - 1);
            }
        }
        return lstOpen.size() == 0; // Et si tout a bien été fermé, la liste doit alors être vide

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
	// Algo de recherche dichotomique de base, avec des limites qui évoluent en fonction de l'élément courant
        int bas = 0;
        int haut = liste.size() - 1;
        int i = 0;
        while(bas < haut)
        {
            i = (bas+haut) / 2;
            if(liste.get(i) < valeur)
                bas = i + 1;
            else
                haut = i;
        }
        return bas < liste.size() && valeur.equals(liste.get(bas));
    }



}
